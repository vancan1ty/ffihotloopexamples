package in.derros.jni;

import java.nio.file.FileSystems;


class Utilities {
    // Ensure library is only loaded once
    static {
        if (System.getProperty("os.name").startsWith("Windows")) {
            // Windows based
            try {
                System.load(
                    FileSystems.getDefault()
                            .getPath("./../build/libjnitests.dll")  // Dynamic link
                            .normalize().toAbsolutePath().toString());
            } catch (UnsatisfiedLinkError e) {
                System.load(
                    FileSystems.getDefault()
                            .getPath("./../build/libjnitests.lib")  // Static link
                            .normalize().toAbsolutePath().toString());
            }
        } else {
            // Unix based
            try {
                System.load(
                    FileSystems.getDefault()
                            .getPath("./../build/libjnitests.so")  // Dynamic link
                            .normalize().toAbsolutePath().toString());
            } catch (UnsatisfiedLinkError e) {
                System.load(
                    FileSystems.getDefault()
                            .getPath("./../build/libjnitests.a")  // Static link
                            .normalize().toAbsolutePath().toString());
            }
        }
    }

    private native void printMethod();
    private native long addOne(long a);
    private native long loopToMax(long start, long max);
    private native boolean trueFalse();
    private native int power(int b, int e);
    private native byte[] returnAByteArray();
    private native String stringManipulator(String s, String[] s1);

    public void printUtil() { printMethod();  }
    public boolean boolTest() { return trueFalse();  }
    public int pow(int b, int e) { return power(b, e); }
    public byte[] testReturnBytes() { return returnAByteArray(); }
    public String manipulateStrings(String s, String[] s1) { return stringManipulator(s, s1);  }

    static long LOOPMAX = 2000000000L;

    public static void main(String[] args) {
        Utilities util = new Utilities();
        long st1 = System.currentTimeMillis();
        long counter = 0;
        while(counter < LOOPMAX) {
            counter = util.addOne(counter);
            if(counter % 1000000 == 0) {
                counter = counter + 10;
            }
        }
        long et1 = System.currentTimeMillis();

        long st2 = System.currentTimeMillis();
        long counter2 = 0;
        while(counter2 < LOOPMAX) {
            counter2 = counter2 + 1;
            if(counter2 % 1000000 == 0) {
                counter2 = counter2 + 10;
            }
        }
        long et2 = System.currentTimeMillis();

        long st3 = System.currentTimeMillis();
        long counter3 = util.loopToMax(0,LOOPMAX);
        long et3 = System.currentTimeMillis();

        double tt1 = ((et1-st1)/1000.0);
        double tt2 = ((et2-st2)/1000.0);
        double tt3 = ((et3-st3)/1000.0);
        System.out.println("t1: " + tt1 + " to find " + counter + " (" + String.format("%.3f",(LOOPMAX/tt1)) + " per sec)");
        System.out.println("t2: " + tt2 + " to find " + counter2 + " (" + String.format("%.3f",(LOOPMAX/tt2)) + " per sec)") ;
        System.out.println("t3: " + tt3 + " to find " + counter3 + " (" + String.format("%.3f",(LOOPMAX/tt3))  + " per sec)");

//        util.printUtil();
//        System.out.println(util.boolTest() + "\n");
//        System.out.println(util.pow(2, 2) + "\n\n");
//        byte[] bs = util.testReturnBytes();
//        for ( byte b : bs ) { System.out.println("A Byte is: " + b);  }
//        System.out.println("THIS IS THE STRING MANIPULATOR!!");
//        System.out.println(
//                util.manipulateStrings("asdfxvcbiojdasaisdf hello world,,,",
//                args));
    }
}
