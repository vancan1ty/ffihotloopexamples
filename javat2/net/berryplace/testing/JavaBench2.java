package net.berryplace.testing;

import java.lang.foreign.Linker;
import java.lang.foreign.FunctionDescriptor;
import java.lang.foreign.SymbolLookup;
import java.lang.foreign.ValueLayout;
import java.nio.file.Paths;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodType;

public class JavaBench2 {

    private static final SymbolLookup libLookup;
    static String libPath = "simple.so";

    static {
        // loads a particular C library
        System.load(Paths.get("").toAbsolutePath().toString() + "/" + libPath);
        libLookup = SymbolLookup.loaderLookup();
    }

    public static MethodHandle getaddOneMethod() {
        Linker linker          = Linker.nativeLinker();
        var addOneMethod = libLookup.find("addOne");

        if (addOneMethod.isPresent()) {
            var methodReference = linker
                    .downcallHandle(
                            addOneMethod.get(),
                            FunctionDescriptor.of(ValueLayout.JAVA_LONG, ValueLayout.JAVA_LONG)
                    );

            return methodReference;

        }
        throw new RuntimeException("addOne function not found.");
    }

    static long LOOPMAX = 2000000000L;

    public static void main(String[] args) throws Throwable {
        MethodHandle addOne = getaddOneMethod();

        long st1 = System.currentTimeMillis();
        long counter = 0;

        while(counter < LOOPMAX) {
            counter = (long) addOne.invoke(counter);
            if(counter % 1000000 == 0) {
                counter = counter + 10;
            }
        }
        long et1 = System.currentTimeMillis();

        double tt1 = ((et1-st1)/1000.0);
        System.out.println("t1: " + tt1 + " to find " + counter + " (" + String.format("%.3f",(LOOPMAX/tt1)) + " per sec)");
    }
}