<?php
// create FFI object, loading libc and exporting function printf()
$ffi = FFI::cdef("
    long addOne(long valIn);
    long loopToMax(long start, long max);
    ",
    "./simple.so");
// call C's printf()

        // $LOOPMAX = 2000000000;
        $LOOPMAX    = 2000000000;

$st1 = microtime(true);
        $counter = 0;
        while($counter < $LOOPMAX) {
            $counter = $ffi->addOne($counter);
            if($counter % 1000000 == 0) {
                $counter = $counter + 10;
            }
        }
        $et1 = microtime(true);

        $st2 = microtime(true);
        $counter2 = 0;
        while($counter2 < $LOOPMAX) {
            $counter2 = $counter2 + 1;
            if($counter2 % 1000000 == 0) {
                $counter2 = $counter2 + 10;
            }
        }
        $et2 = microtime(true);

        $st3 = microtime(true);
        $counter3 = $ffi->loopToMax(0,$LOOPMAX);
        $et3 = microtime(true);

        $tt1 = (($et1-$st1));
        $tt2 = (($et2-$st2));
        $tt3 = (($et3-$st3));
        print "t1: $tt1 to find $counter  (" . sprintf("%.3f",($LOOPMAX/$tt1)) . " per sec)\n";
        print "t2: $tt2 to find $counter2 (" . sprintf("%.3f",($LOOPMAX/$tt2)) . " per sec)\n";
        print "t3: $tt3 to find $counter3 (" . sprintf("%.3f",($LOOPMAX/$tt3)) . " per sec)\n";

?>
