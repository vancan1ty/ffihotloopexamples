#include "utility.h"

long addOne (long valIn) {
  return 1+valIn;
}

long loopToMax(long start, long max) {
      long counter2 = start;
      while(counter2 < max) {
          counter2 = counter2 + 1;
          if(counter2 % 1000000 == 0) {
            counter2 = counter2 + 10;
          }
      }
      return counter2;
  }

