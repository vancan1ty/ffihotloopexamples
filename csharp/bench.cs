using System;
using System.Runtime.InteropServices;

public class Bench
{
    // Import user32.dll (containing the function we need) and define
    // the method corresponding to the native function.
    [DllImport("../../../simple.so")]
    private static extern long addOne (long valIn);

    [DllImport("../../../simple.so")]
    private static extern long loopToMax(long start, long max);

    public static long CurrentMillis()
    {
       long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
       return milliseconds;
    }

    public static long LOOPMAX = 2000000000;
    

    public static void Main(string[] args)
    {

        long st1 = CurrentMillis();
         long counter = 0;
         while(counter < LOOPMAX) {
             counter = addOne(counter);
             if(counter % 1000000 == 0) {
                 counter = counter + 10;
             }
         }
         long et1 = CurrentMillis();

         long st2 = CurrentMillis();
         long counter2 = 0;
         while(counter2 < LOOPMAX) {
             counter2 = counter2 + 1;
             if(counter2 % 1000000 == 0) {
                 counter2 = counter2 + 10;
             }
         }
         long et2 = CurrentMillis();

         long st3 = CurrentMillis();
         long counter3 = loopToMax(0,LOOPMAX);
         long et3 = CurrentMillis();

         double tt1 = ((et1-st1)/1000.0);
         double tt2 = ((et2-st2)/1000.0);
         double tt3 = ((et3-st3)/1000.0);
         Console.WriteLine("t1: " + tt1 + " to find " + counter + " (" + String.Format("{0}",(LOOPMAX/tt1)) + " per sec)");
         Console.WriteLine("t2: " + tt2 + " to find " + counter2 + " (" + String.Format("{0}",(LOOPMAX/tt2)) + " per sec)") ;
         Console.WriteLine("t3: " + tt3 + " to find " + counter3 + " (" + String.Format("{0}",(LOOPMAX/tt3))  + " per sec)");
    }
}
