package main

/*
// #include "utility.h"
#cgo CFLAGS: -I. -g3 -Wall
   long addOne (long valIn) {
    return 1+valIn;
  }

*/
import "C"

import "fmt"

func main() {
    var LOOPMAX C.long = 2000000000

    var counter C.long = 0;

    for counter < LOOPMAX {
        counter =  C.addOne(counter);
        if (counter % 1000000 == 0) {
            counter = counter + 10;
        }
    }
    fmt.Println(counter);
}